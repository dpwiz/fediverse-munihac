module Fediverse.HTTP where

import Data.Aeson ((.=))
import Data.Text (Text)
import Data.Traversable (for)
import Data.XRD.Types (XRD(..), emptyXRD)

import qualified Data.Aeson as Aeson
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as BSL
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text
import qualified URI.ByteString as URI
import qualified Data.XRD.Types as XRD
import qualified Data.XRD.JSON as XRD
import qualified Network.HTTP.Types as HTTP
import qualified Network.Wai as WAI
import qualified Network.Wai.Handler.Warp as Warp

warp :: Warp.Port -> IO ()
warp port = Warp.run port wai

wai :: WAI.Application
wai request respond = do
  print $ WAI.requestMethod request
  print $ WAI.requestHeaderHost request
  print $ WAI.requestHeaders request
  print $ WAI.pathInfo request
  print $ WAI.queryString request
  putStrLn ""

  case WAI.pathInfo request of
    [] ->
      serveIndex
    [".well-known", "host-meta"] ->
      serveHostMeta
    [".well-known", "webfinger"] ->
      serveWebFinger
    ["users", username] ->
      case lookup "Accept" (WAI.requestHeaders request) of
        Nothing ->
          serveIndex
        Just (Text.splitOn ", " . Text.decodeUtf8 -> accepts)
          | "application/activity+json" `elem` accepts ->
            serveActivityJson username
    ["users", username, "outbox"] ->
      serveOutbox username
    _ ->
      serveNotFound
  where
    serveIndex =
      respond $ WAI.responseLBS HTTP.status200 [] "Hello World"

    serveHostMeta = do
      host <- getHost request
      respond . WAI.responseLBS HTTP.status200 [] $ mconcat
        [ "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
        , "<XRD xmlns=\"http://docs.oasis-open.org/ns/xri/xrd-1.0\">"
        , "  <Link rel=\"lrdd\" type=\"application/xrd+xml\" template=\"https://"<> host <> "/.well-known/webfinger?resource={uri}\"/>"
        , "</XRD>"
        ]

    serveWebFinger =
      case lookup "resource" (WAI.queryString request) of
        Just (Just resource) ->
          webfinger (Text.decodeUtf8 resource) -- request respond
        _ ->
          serveBadRequest "query should be ?resource=acct:username@hostname.tld"

    serveActivityJson username = do
      host <- fmap (Text.decodeUtf8 . BSL.toStrict) (getHost request)
      serveJson $ getPerson username host

    serveOutbox username = do
      host <- fmap (Text.decodeUtf8 . BSL.toStrict) (getHost request)
      case lookup "page" (WAI.queryString request) of
        Nothing ->
          serveJson $ getOutbox username host
        Just _ ->
          serveJson $ getOutboxPage username host

    serveNotFound =
      respond $ WAI.responseLBS HTTP.status404 [] "Nothing here."

    serveBadRequest err =
      respond $ WAI.responseLBS HTTP.status404 [] err

    serveBadResponse err =
      respond $ WAI.responseLBS HTTP.status404 [] err

    serveJson :: IO (Either BSL.ByteString Aeson.Value) -> IO WAI.ResponseReceived
    serveJson proc =
      proc >>= \case
        Left err ->
          serveBadResponse err
        Right doc ->
          respond $
            WAI.responseLBS
              HTTP.status200
              [("Content-Type", "application/jrd+json; charset=utf-8")]
              (Aeson.encode doc)

    -- TODO: those should be top-level
    getHost :: WAI.Request -> IO BSL.ByteString
    getHost request = case WAI.requestHeaderHost request of
      Just bs ->
        pure $ BSL.fromStrict bs
      Nothing ->
        fail "assert: the Host header is set"

    -- webfinger :: BS.ByteString -> WAI.Application
    webfinger resource = do -- request respond =
      host <- getHost request
      getXrd resource (Text.decodeUtf8 $ BSL.toStrict host) >>= \case
        Left err ->
          serveBadResponse err
        Right xrd ->
          respond $
            WAI.responseLBS
              HTTP.status200
              [("Content-Type", "application/jrd+json; charset=utf-8")]
              (XRD.toByteString xrd)

getXrd :: Text -> Text -> IO (Either BSL.ByteString XRD)
getXrd subject host =
  pure $ case uriParsed of
    Left err ->
      Left . mappend "getXrd: " . BSL.pack $ show err
    Right (xrdSubject, xrdAliases, xrdLinks) ->
      pure emptyXRD{xrdSubject, xrdAliases, xrdLinks}
  where
    uriParsed = do
      subj <- XRD.subject subject
      (user, server) <- case subj of
        XRD.Subject URI.URI{uriPath} ->
          case BS.break (== '@') uriPath of
            (user, BS.drop 1 -> server) ->
              pure (Text.decodeUtf8 user, Text.decodeUtf8 server)

      self <- XRD.subject $ "https://" <> server <> "/users/" <> user
      profile <- XRD.subject $ "https://" <> server <> "/@" <> user

      selfActivity <- do
        let rel = XRD.LinkRelRegistered "self"
        let typ = XRD.LinkType "application/activity+json"
        href <- XRD.uri $ mconcat [ "https://", server, "/users/", user ]

        pure XRD.emptyLink
          { XRD.linkRel = Just rel
          , XRD.linkType = Just typ
          , XRD.linkHref = Just href
          }

      htmlProfile <- do
        rel <- XRD.LinkRelURI <$> XRD.uri "http://webfinger.net/rel/profile-page"
        let typ = XRD.LinkType "text/html"
        href <- XRD.uri $ mconcat [ "https://", server, "/@", user ]

        pure XRD.emptyLink
          { XRD.linkRel = Just rel
          , XRD.linkType = Just typ
          , XRD.linkHref = Just href
          }

      -- -- XXX: this one is optional
      -- atomUpdates <- do
      --   rel <- XRD.linkRelURI "http://schemas.google.com/g/2010#updates-from"
      --   let typ = XRD.LinkType "application/atom+xml"
      --   href <- XRD.uri $ mconcat [ "https://", server, "/users/", user, "/atom.xml" ]

      --   pure XRD.emptyLink
      --     { XRD.linkRel = Just rel
      --     , XRD.linkType = Just typ
      --     , XRD.linkHref = Just href
      --     }

      pure (Just subj, [self, profile], [selfActivity, htmlProfile])

getPerson :: Text -> Text -> IO (Either BSL.ByteString Aeson.Value)
getPerson username host = pure . Right $ Aeson.object
  [ "@context" .=
      [ Text.pack "https://www.w3.org/ns/activitystreams"
      , "https://w3id.org/security/v1"
      ]
  , "id" .= userPrefix
  , "type" .= Text.pack "Person"

  , "inbox" .= userPrefixed "/inbox"
  , "outbox" .= userPrefixed "/outbox"
  -- , "featured" .= userPrefixed "/featured"

  , "preferredUsername" .= username
  , "name" .= (username <> " at the " <> host)
  , "summary" .= Text.pack "<p>html-ish summary</p>"
  , "url" .= hostPrefixed ("/@" <> username)

  , "endpoints" .= Aeson.object
      [ "sharedInbox" .= hostPrefixed "/inbox"
      ]
  ]
  where
    hostPrefix = "https://" <> host
    hostPrefixed = mappend hostPrefix

    userPrefix = hostPrefixed $ "/users/" <> username
    userPrefixed = mappend userPrefix

getOutbox :: Text -> Text -> IO (Either BSL.ByteString Aeson.Value)
getOutbox username host = pure . Right $ Aeson.object
    [ "@context" .= Text.pack "https://www.w3.org/ns/activitystreams"
    , "id" .= userPrefixed "/outbox"
    , "type" .= ("OrderedCollection" :: Text)
    , "totalItems" .= (2 :: Int)
    , "first" .= userPrefixed "/outbox?page=true"
    , "last" .=  userPrefixed "/outbox?page=true&min_id=0"
    ]
  where
    hostPrefix = "https://" <> host
    hostPrefixed = mappend hostPrefix

    userPrefix = hostPrefixed $ "/users/" <> username
    userPrefixed = mappend userPrefix

getOutboxPage :: Text -> Text -> IO (Either BSL.ByteString Aeson.Value)
getOutboxPage username host = pure $ Right orderedCollectionPage
  where
    hostPrefix = "https://" <> host
    hostPrefixed = mappend hostPrefix

    userPrefix = hostPrefixed $ "/users/" <> username
    userPrefixed = mappend userPrefix

    published = Text.pack "2019-09-06T09:15:23Z"

    asContext = Text.pack "https://www.w3.org/ns/activitystreams"
    asOrderedCollectionPage = Text.pack "OrderedCollectionPage"
    asPublic = Text.pack "https://www.w3.org/ns/activitystreams#Public"
    asNote = Text.pack "Note"

    orderedCollectionPage = Aeson.object
      [ "@context" .= asContext
      , "id" .= userPrefixed "/outbox"
      , "type" .= asOrderedCollectionPage
      , "prev" .= userPrefixed "/outbox?min_id=1&page=true"
      , "partOf" .= userPrefixed "/outbox"
      , "orderedItems" .=
          [ createNote1
          , createNote2
          ]
      ]

    create :: Text -> Aeson.Value -> Aeson.Value
    create oid o = Aeson.object
      [ "id" .= userPrefixed ("/statuses/" <> oid <> "/activity" )
      , "type" .= ("Create" :: Text)
      , "actor" .= userPrefix
      , "published" .= published
      , "to" .= [asPublic]
      , "object" .= o
      ]

    createNote1 = create "1" $ Aeson.object
      [ "id" .= userPrefixed ("/statuses/" <> "1")
      , "type" .= asNote
      , "summary" .= Aeson.Null
      , "content" .= Text.pack "This is the first Note."
      , "published" .= published
      ]

    createNote2 = create "2" $ Aeson.object
      [ "id" .= userPrefixed ("/statuses/" <> "2")
      , "type" .= asNote
      , "summary" .= Aeson.Null
      , "content" .= Text.pack "This is the second Note. It is different."
      , "published" .= published
      ]
